import { Scene, assignRole, detachRoles } from '../drama'

interface BookATrainActors {
  customer: {
    id: string
  }
  operator: {
    proposeStartingAndDestinationStations: (startingAndDestinationStations: { id: string; name: string }[]) => void
    proposeBookableTrainList: (bookableTrains: { id: string; startSchedule: Date; endSchedule: Date }[]) => void
    askForSelectableNumberOfSeats: () => void
    askForBookedJourneyConfirmation: () => void
    confirmBookingRegistration: (bookingInformation: {
      startingStation: { id: string; name: string }
      destinationStation: { id: string; name: string }
      train: { id: string }
      seats: { seatNumber: number; trainCar: { id: string; name: string } }[]
      customer: { id: string }
    }) => void
  }
  transitService: {
    getOperatingStations: () => { id: string; name: string }[]
    getOperatingTrains: (date: Date) => { id: string; startSchedule: Date; endSchedule: Date }[]
  }
  logisticsService: {
    checkSeatsAvailability: (numberOfSeats: number) => boolean
  }
  salesService: {
    registerBooking: (bookingOrder: {}) => void // TODO: Check with the sales team if this is the right type
  }
}

interface BookATrainPublicApi {
  askToBookAJourney: () => void
  confirmDateAndStartingAndDestinationStations: (
    date: Date,
    startStation: { id: string; name: string },
    destinationStation: { id: string; name: string }
  ) => void
  confirmSelectedTrain: (train: { id: string }) => void
  confirmNumberOfSeats: (numberOfSeats: number) => void
  confirmBooking: () => void
}

export const BookATrain = Scene(bookATrain)
export function bookATrain({
  customer,
  operator,
  transitService,
  logisticsService,
  salesService,
}: BookATrainActors): BookATrainPublicApi {
  let JourneyInformation: JourneyInformationType
  type JourneyInformationType = {
    startingStation: { id: string; name: string }
    destinationStation: { id: string; name: string }
    train: { id: string }
    seats: { seatNumber: number; trainCar: { id: string; name: string } }[]
    customer: { id: string }
  }
  function updateJourneyInformation<K extends keyof JourneyInformationType>(
    key: K,
    value: JourneyInformationType[K]
  ): void {
    JourneyInformation[key] = value
  }

  // #region CUSTOMER ROLE
  const Customer = customer

  // #region OPERATOR ROLE
  const proposeStartingAndDestinationStations = Symbol()
  const aknwoledgeStartingAndDestinationStations = Symbol()
  const aknwoledgeSelectedTrain = Symbol()
  const checkSeatsAvailability = Symbol()
  const aknowledgeCustomerBookingOrderConfirmation = Symbol()
  const confirmBookingRegistrationToCustomer = Symbol()

  const Operator = assignRole(operator, {
    [proposeStartingAndDestinationStations]: () => {
      updateJourneyInformation('customer', Customer)

      const operatingStations = TransitService[getOperatingStations]()

      Operator.proposeStartingAndDestinationStations(operatingStations)
    },

    [aknwoledgeStartingAndDestinationStations]: (
      date: Date,
      startingPoint: { id: string; name: string },
      destinationPoint: { id: string; name: string }
    ) => {
      updateJourneyInformation('startingStation', startingPoint)
      updateJourneyInformation('destinationStation', destinationPoint)

      const trains = TransitService[getOperatingTrains](date)
      Operator.proposeBookableTrainList(trains)
    },

    [aknwoledgeSelectedTrain]: (train: { id: string }) => {
      updateJourneyInformation('train', train)

      Operator.askForSelectableNumberOfSeats()
    },

    [checkSeatsAvailability]: (numberOfSeats: number) => {
      if (LogisticsService[confirmSeatsAvailability](numberOfSeats)) {
        updateJourneyInformation('seats', [{ seatNumber: 34, trainCar: { id: '34', name: '34' } }])

        Operator.askForBookedJourneyConfirmation()
      }
    },

    [aknowledgeCustomerBookingOrderConfirmation]: () => {
      SalesService[registerBooking](JourneyInformation)
    },

    [confirmBookingRegistrationToCustomer]: () => {
      Operator.confirmBookingRegistration(JourneyInformation)

      detachRoles([Operator, TransitService, LogisticsService, SalesService])
    },
  })
  // #endregion

  // #region TRANSIT SERVICE ROLE
  const getOperatingStations = Symbol()
  const getOperatingTrains = Symbol()

  const TransitService = assignRole(transitService, {
    [getOperatingStations]: () => {
      const operatingStations = TransitService.getOperatingStations()

      return operatingStations
    },

    [getOperatingTrains]: (date: Date) => {
      const operatingTrains = TransitService.getOperatingTrains(date)

      return operatingTrains
    },
  })
  // #endregion

  // #region LOGISTICS ROLE
  const confirmSeatsAvailability = Symbol()

  const LogisticsService = assignRole(logisticsService, {
    [confirmSeatsAvailability]: (numberOfSeats: number) => {
      return LogisticsService.checkSeatsAvailability(numberOfSeats)
    },
  })
  // #endregion

  // #region SALES SERVICE ROLE
  const registerBooking = Symbol()

  const SalesService = assignRole(salesService, {
    [registerBooking]: (booking: JourneyInformationType) => {
      SalesService.registerBooking(booking)

      Operator[confirmBookingRegistrationToCustomer]()
    },
  })
  // #endregion

  // #region PUBLIC API
  {
    return {
      askToBookAJourney() {
        Operator[proposeStartingAndDestinationStations]()
      },
      confirmDateAndStartingAndDestinationStations(date, startStation, destinationStation) {
        Operator[aknwoledgeStartingAndDestinationStations](date, startStation, destinationStation)
      },
      confirmSelectedTrain(train) {
        Operator[aknwoledgeSelectedTrain](train)
      },
      confirmNumberOfSeats(numberOfSeats) {
        Operator[checkSeatsAvailability](numberOfSeats)
      },
      confirmBooking() {
        Operator[aknowledgeCustomerBookingOrderConfirmation]()
      },
    }
  }
  // #endregion
}
