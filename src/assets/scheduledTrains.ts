function getRandomTrainNumber() {
  return Math.floor(Math.random() * 9000) + 1000;
}

function getRandomDate(): Date {
  const startDate = new Date();
  const daysToAdd = Math.floor(Math.random() * 10) + 1;
  const hoursToAdd = Math.floor(Math.random() * 24);
  const minutesToAdd = Math.floor(Math.random() * 60);
  const secondsToAdd = Math.floor(Math.random() * 60);
  const msToAdd = Math.floor(Math.random() * 1000);

  return new Date(
    startDate.getTime() +
      daysToAdd * 24 * 60 * 60 * 1000 +
      hoursToAdd * 60 * 60 * 1000 +
      minutesToAdd * 60 * 1000 +
      secondsToAdd * 1000 +
      msToAdd
  );
}

function addRandomTime(date: Date): Date {
  const newDate = new Date(date.getTime());
  const hoursToAdd = Math.floor(Math.random() * 6) + 1;
  newDate.setHours(newDate.getHours() + hoursToAdd);
  return newDate;
}

function getRandomCity(): string {
  const cities = [
    "Paris",
    "Turin",
    "Berlin",
    "Bruges",
    "Bordeaux",
    "Marseille",
  ];
  const randomIndex = Math.floor(Math.random() * cities.length);
  return cities[randomIndex];
}

export type Train = {
  numberId: number;
  departureDate: Date;
  arrivalDate: Date;
  destination: string;
};

export const mockTrains: Train[] = Array.from({ length: 100 }, () => {
  const date = getRandomDate();

  return {
    numberId: getRandomTrainNumber(),
    departureDate: date,
    arrivalDate: addRandomTime(date),
    destination: getRandomCity(),
  };
});
