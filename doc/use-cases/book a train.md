# Use Case: Customer Booking a Train

Primary Actor: Customer

Stakeholders:

Customer: The individual or organization who wants to book a train ticket.
Sales operator: Company's outward facing service that is responsible of selling tickets to customers.
Transit service: Company's internal service that is responsible of choosing which trains will transit.
Internal Logistics: Company's internal service that's responsible to maximize the used seats in each trains.

Preconditions:

The customer has access to the train booking platform or application.

## Main Success Scenario:

1 - Customer emits the need to take a journey to the train Operator.
2 - Train Operator asks the Transit service which stations compose the national network.
3 - Transit service provides a list of all the national network stations to the Train Operator.
4 - Train Operator proposes multiple starting points, destinations, and a date to book the train to the Customer.
5 - Customer selects a starting point, a destination, and a date, and communicates them to the Train Operator.
6 - Train Operator asks the Transit service which trains are transiting from the starting station to the ending station on the specified date.
7 - Transit service provides a list of the trains corresponding to the aforementioned criteria to the Train Operator.
8 - Train Operator proposes these next bookable trains to the Customer.
9 - Customer selects a train between the proposed options and communicates her choice to the Train Operator.
10 - Train Operator asks the Customer how many seats are needed.
11 - Customer answers the Train Operator with the number of required seats.
12 - Train Operator asks the Logistics if the chosen train has the number of required seats.
13 - Logistics confirms that the number of required seats is available to the Train Operator.
14 - Train Operator confirms the availability of the booking to the Customer.
15 - Train Operator sends the booking information to the Customer for review.
16 - Customer reviews the final information and confirms the booking to the Train Operator.
17 - Train Operator sends booking information to the Sales service for registration.
18 - Sales service confirm the registration has succesfull to Train Operator.
19 - Train Operator confirm the registration to Customer.
